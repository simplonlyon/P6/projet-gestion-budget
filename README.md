# projet-gestion-budget

Réaliser avec Angular une application permettant de gérer un budget (thème libre).
Les fonctionnalités attendues sont :
* CRUD des opération (entrée/sorties d'argent)
* Catégorisation des opérations
* Plusieurs affichages pour les opérations (exemples: total par catégories sur un mois/an, balance entrée/sortie sur une période, etc.), éventuellement sous forme de graphiques (en utilisant une librairie angular)

Selon le thème de votre application vous pouvez (et êtes encouragé.e.s à) rajouter d'autres fonctionnalités.
Pensez bien à faire une petite maquettes fonctionnelles des différents écrans et à identifier vos entités avant toute chose.

En terme de technologie vous devrez avoir 2 applications : 
* Une application front-end faites avec Angular. Utilisez plusieurs components plutôt qu'un mega-component. 
Les appels ajax seront faits dans des services dédiés. La pagination faite avec le router angular
* Une application back-end, une API REST qui permettra de faire les différentes requêtes http pour faire le CRUD des entités.
Il est recommandé de faire le back-end en utilisant Symfony 4 mais pour les plus aventureu.x.ses vous pouvez également le faire avec node+express.js



Exemples d'idées de thème : Comptes en ligne (genre l'application Bankin'), Prévision de budget voyage/vacances, budget entre ami.e.s.

## Mise en ligne

I. L'API REST
1. Se connecter à simplonlyon.fr en ssh
2. Cloner votre projet Symfony dans votre dossier www dans un dossier api-budget
3. Aller dans ce dossier et faire un `composer install`
4. Créer la paire clef privée/publique en utilisant ce script : https://gitlab.com/simplonlyon/P6/api-contact/blob/master/generate-keys.sh
5. Se connecter à mysql (`mysql -u username -p` et votre username comme mdp) et faire un `CREATE DATABASE username_budget;` avec votre username
6. Editer le fichier .env pour mettre les informations de connexion du serveur  
7. Migrer la base de données avec un `php bin/console doctrine:migrations:migrate`


II. L'Angular
1. Dans le fichier src/environments/environments.ts rajouter une propriété "backUrl" dans l'objet environment avec comme valeur "http://localhost:8080"
2. Dans le fichier src/environments/environments.prod.ts faire pareil, mais mettre comme valeur à backUrl "https://www.simplonlyon.fr/promo6/username/api-budget" où username est votre username simplonlyon
3. Dans vos services, faire un import de la variable environment et remplacer le morceau de lien concerné pour votre apiUrl
4. Faire un ng build --prod
5. Editer le fichier index.html obtenu dans le dossier dist et changer le base href qui de base est à "/" pour mettre à la place "https://www.simplonlyon.fr/promo6/username/nom-projet-angular/"
6. Transférer le dossier dist sur simplonlyon.fr avec un scp. Exemple : depuis un terminal local (non connecté à simplonlyon) `scp -r dist/project-name username@simplonlyon.fr:www`
7. Renommer le dossier si besoin côté simplonlyon.fr 
